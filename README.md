# README #

Once the real estate agent has confirmed his interest in Onpreo by confirming his email id, redirect the agent to the 
onboarding landing page. This page should consist of a short form with an interactive slider indicating the completion 
of the form details. Once the form is submitted, the agent should be redirected to the workspace/dashboard.

### What is this repository for? ###

Create an interactive onboarding process for the agent

### How do I get set up? ###

*Include a short form to complete agent details. Form should contain
i) Name
ii) Role
iii) Address
iv) Phone Number

*Subsequently create a workspace dashboard for the agent consisting of 
i) A Welcome message with the agent's name
ii) A quick link of the agent's website for him/her to share
iii) Possible include buttons with different sector names such as finance, real estate etc for the agent to choose where he wishes to be redirected to
iv) Maybe use a pop up to remind the agent to input his payment details. Possibly provide some kind of marketting trick such as discount message on the pop up
to lure him to buy the premium subscription.

### Contribution guidelines ###

1)Writing Prototype

2)Code review

3)Code testing